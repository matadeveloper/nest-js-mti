import { Prisma } from './prisma.utils';
import groups from './groups.seeds';

async function main() {
  console.log('\n-> START::SEEDING');

  await groups.run();
  console.log('   └─ Completed: Groups.');

  console.log('-> FINISH::SEEDING\n');
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await Prisma.instance.$disconnect();
  });
