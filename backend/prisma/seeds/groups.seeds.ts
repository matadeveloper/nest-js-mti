import { Prisma } from './prisma.utils';
import * as groupsJson from './group.json';
import { Group } from '@prisma/client';

export const run = async () => {
  return Prisma.instance.group.createMany({
    data: groupsJson as Group[],
    skipDuplicates: true
  });
};

export default {
  run,
};