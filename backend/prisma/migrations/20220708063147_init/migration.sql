-- DropForeignKey
ALTER TABLE "menus_to_group" DROP CONSTRAINT "menus_to_group_menu_id_fkey";

-- AddForeignKey
ALTER TABLE "menus_to_group" ADD CONSTRAINT "menus_to_group_menu_id_fkey" FOREIGN KEY ("menu_id") REFERENCES "menus"("id") ON DELETE CASCADE ON UPDATE CASCADE;
