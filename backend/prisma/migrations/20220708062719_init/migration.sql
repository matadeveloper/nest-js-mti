-- CreateEnum
CREATE TYPE "TypeMenu" AS ENUM ('Menu', 'label');

-- CreateTable
CREATE TABLE "menus" (
    "id" SERIAL NOT NULL,
    "label" TEXT,
    "type" "TypeMenu" NOT NULL,
    "icon_color" TEXT,
    "link" TEXT,
    "sort" INTEGER NOT NULL,
    "parent" INTEGER NOT NULL DEFAULT 0,
    "icon" TEXT,
    "menu_type_id" INTEGER NOT NULL,
    "active" INTEGER NOT NULL,

    CONSTRAINT "menus_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "menus_to_group" (
    "menu_id" INTEGER NOT NULL,
    "group_id" INTEGER NOT NULL,

    CONSTRAINT "menus_to_group_pkey" PRIMARY KEY ("menu_id","group_id")
);

-- CreateTable
CREATE TABLE "menus_type" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "definition" TEXT NOT NULL,

    CONSTRAINT "menus_type_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "menus" ADD CONSTRAINT "menus_menu_type_id_fkey" FOREIGN KEY ("menu_type_id") REFERENCES "menus_type"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "menus_to_group" ADD CONSTRAINT "menus_to_group_group_id_fkey" FOREIGN KEY ("group_id") REFERENCES "groups"("group_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "menus_to_group" ADD CONSTRAINT "menus_to_group_menu_id_fkey" FOREIGN KEY ("menu_id") REFERENCES "menus"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
