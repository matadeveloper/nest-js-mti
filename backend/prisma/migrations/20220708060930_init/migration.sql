-- CreateTable
CREATE TABLE "settings" (
    "setting_id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "value" TEXT,
    "created_at" TIMESTAMP(3),
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "settings_pkey" PRIMARY KEY ("setting_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "settings_name_key" ON "settings"("name");
