## Judul
Matadeveloper Admin

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

# seed prisma
$ npx prisma db seed

# run prisma
$ npx prisma init

# migrate prisma
$ npx prisma migrate dev --name init

# prisma db pull
$ npx prisma db pull

# prisma db push
$ npx prisma db push

# seed prisma
$ npx prisma db seed
```
## License
Nest is [MIT licensed](LICENSE).
