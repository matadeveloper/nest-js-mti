There are three main primitives in JavaScript and TypeScript.

boolean - true or false values
number - whole numbers and floating or int point values
string - text values like "TypeScript Rocks"

TypeScript Special Types
------------------------------------------------------------
Type: any
any is a type that disables type checking and effectively allows all types to be used.
Example with any
let v: any = true;
v = "string"; // no error as it can be "any" type
Math.round(v); // no error as it can be "any" type