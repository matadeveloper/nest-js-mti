let Str : string = "Hello World";
let Num : number = 123;
let Bool : boolean = true;
// console.log(Str)
// console.log(Num+Str)

/* Set in interfaces */
interface User {
    name: string;
    id: number;
}

const user : User = {
    name : 'Maulana',
    id : 2
}

// console.log(user)

/* Special Types */
let v: any = true;
v = "string";
// console.log(v)

// define our tuple
let ourTuple: [number, boolean, string];
ourTuple = [5, false, 'Coding God was here'];
// We have no type safety in our tuple for indexes 3+
ourTuple.push('Something new and wrong');
// console.log(ourTuple);

/* TypeScript Object Types */
const car: { type: string, model: string, year: number } = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
};
// console.log(car)

/* Typescript for require variable */
const car1: { type: string, mileage?: number } = { // no error
    type: "Toyota"
};
car1.mileage = 2000;
// console.log(car1)


function greet(person: string | number, date: Date) {
    console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}
   
// greet(1,new Date())

enum dataUser {
    algobash = 100,
    alternatif
}
// console.log(dataUser.algobash)

interface Maulana{
    name : string,
    fullname : string,
    lastname?: string // get enter variable or not
}

// Fullstring
const Data1 : Maulana = {
    name : 'Maulana',
    fullname : 'Pasaribu',
    lastname : 'OK'
}
// console.log(Data1)

// Partialstring
const Data2 : Maulana = {
    name : 'Maulana',
    fullname : 'Pasaribu',
    lastname : 'OK'
}

// No type annotations here, but TypeScript can spot the bug
const names = ["Alice", "Bob", "Eve"];
 
// Contextual typing for function
names.forEach(function (s : string) {
    if(s){
        let filterValue = s.toUpperCase();
        // console.log(filterValue);
    }
});

var ObjectTuple : any = {}
ObjectTuple['data'] = 'String'

console.log(ObjectTuple)
